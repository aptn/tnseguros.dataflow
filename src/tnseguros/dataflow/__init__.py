from datetime import date
from datetime import datetime
from xlrd import empty_cell, XL_CELL_DATE, XL_CELL_NUMBER, xldate_as_tuple
from xlrd.xldate import XLDateError
from xlutils.filter import process

import simplejson as json
import sys
import xlrd
import xlwt.Utils
import xlutils.filter


class XLSReader(object):

    file = None
    current_row_index = -1

    def __init__(self, filename, **kwargs):
        self.filename = filename
        if kwargs.has_key('sheet'):
            sheet = kwargs['sheet']
            del kwargs['sheet']
        else:
            sheet = 0
        self.book = xlrd.open_workbook(self.filename, **kwargs)
        self.set_sheet(sheet)

    def set_sheet(self, sheet):
        self.current_sheet = self.book.sheet_by_index(sheet)

    def set_row_index(self, row):
        self.current_row_index = row

    def next(self, skip_empty=False):
        sheet = self.current_sheet
        datemode = self.book.datemode
        any = False
        row = None
        while not any:
            if self.current_row_index >= sheet.nrows - 1:
                row = None
                break

            self.current_row_index = self.current_row_index + 1
            row = sheet.row(self.current_row_index)

            if not skip_empty:
                break

            # Test if the row is empty.
            for cell in row:
                if cell is not empty_cell and cell.value != '':
                    any = True
                    break

        if row is None:
            return [None, None]

        values = []
        for col_idx, cell in enumerate(row):
            if cell is empty_cell or cell.value == '':
                v = None
            elif cell.ctype == XL_CELL_DATE:
                try:
                    v = xldate_as_tuple(cell.value, datemode)
                except XLDateError:
                    return [-1, [self.current_row_index, col_idx, cell.value]]
                if v[-3:] == (0, 0, 0):
                    v = v[:-3]
            elif cell.ctype == XL_CELL_NUMBER:
                v = cell.value
                i = int(v)
                if i == v:
                    v = i
            else:
                v = cell.value
            values.append(v)
        return [self.current_row_index, values]

    def next_not_empty(self):
        return self.next(skip_empty=True)

    def column_count(self):
        return self.current_sheet.ncols

    def close(self):
        pass


class DefaultXLSReader(xlutils.filter.XLRDReader):
    """A XLRDReader that copies only the first sheet twice.

    The copies will have only a limited number of rows from top.
    """

    def __init__(self, wb, filename, limit, double_first=True):
        self.wb = wb
        self.filename = filename
        self.rows_limit = limit
        self.double_first = double_first

    def __call__(self, filter):
        filter.start()
        for workbook, filename in self.get_workbooks():
            filter.workbook(workbook, filename)
            sheet = workbook.sheet_by_index(0)
            if self.double_first:
                sheets = ((0, sheet.name), (1, u'copy'))
            else:
                sheets = ((0, sheet.name),)
            for sheet_x, sheet_name in sheets:
                filter.sheet(sheet, sheet_name)
                for row_x in xrange(min(sheet.nrows, self.rows_limit)):
                    filter.row(row_x, row_x)
                    for col_x in xrange(sheet.ncols):
                        filter.cell(row_x, col_x, row_x, col_x)
            if workbook.on_demand:
                workbook.unload_sheet(0)
        filter.finish()


def _reader_delegator(attr):
    def method(self, *args, **kwargs):
        return getattr(self.reader, attr)(*args, **kwargs)
    return method


class XLSWriter(object):

    def __init__(self, filename, rows_limit,
                 reader=XLSReader,
                 reader_keywords={'formatting_info': True, 'on_demand': True},
                 date_fmt='YYYY-MM-DD',
                 datetime_fmt='YYYY-MM-DD hh:mm',
                 only_preview_sheet=False,
                 filter_reader=DefaultXLSReader):
        self.reader = reader(filename, **reader_keywords)
        self.rbook = self.reader.book
        w = xlutils.filter.XLWTWriter()
        double_first = not only_preview_sheet
        process(filter_reader(self.rbook, 'unknown.xls', rows_limit,
                              double_first=double_first), w)
        self.book = w.output[0][1]
        self.book.active_sheet = 0
        self.preview_sheet = self.book.get_sheet(0)
        self.preview_sheet.selected = True
        self.sheets = {0: self.preview_sheet}
        if not only_preview_sheet:
            self.success_sheet = self.book.get_sheet(1)
            self.success_sheet.selected = False
            self.sheets[1] = self.success_sheet

        self.text_fmt = xlwt.easyxf(num_format_str='@')
        self.date_fmt = xlwt.easyxf(num_format_str=date_fmt)
        self.datetime_fmt = xlwt.easyxf(num_format_str=datetime_fmt)

        # Set the sheet of the reader book.
        self.set_sheet(0)

    def set_sheet_name(self, sheet, name):
        name = name[:31]
        sheet = self.sheets[sheet]
        if xlwt.Utils.valid_sheet_name(name):
            sheet.name = name

    def set_row(self, sheet, idx, orig_idx, values):
        sheet = self.sheets[sheet]
        row = sheet.row(idx)
        original_sheet = self.rbook.sheet_by_index(0)
        for i, value in enumerate(values):
            if value == '':
                row.set_cell_blank(i)
                continue
            elif value is None:
                # Look for a value in the original book.
                try:
                    cell = original_sheet.cell(orig_idx, i)
                except IndexError:
                    # Error: just skip this cell.
                    continue
                if cell.ctype == XL_CELL_DATE:
                    fmt, value = self._get_format_and_value_for_date_cell(cell)
                    row.set_cell_number(i, value, fmt)
                    continue
                value = cell.value
            elif isinstance(value, (list, tuple)):
                self._set_packed_value(row, i, value)
                continue
            elif isinstance(value, basestring):
                # Explicitly set the format to text_fmt.
                row.set_cell_text(i, value, self.text_fmt)
                continue
            row.write(i, value)

    def set_cell(self, sheet, row, col, value):
        sheet = self.sheets[sheet]
        row = sheet.row(row)
        if isinstance(value, (list, tuple)):
            return self._set_packed_value(row, col, value)
        # Get the style of the old cell.
        try:
            old_cell_obj = row._Row__cells[col]
        except IndexError:
            old_xf_idx = None
        else:
            try:
                old_xf_idx = old_cell_obj.xf_idx
            except AttributeError:
                old_xf_idx = None
        # Write the cell as normal.
        row.write(col, value)
        # If an old cell was there and a style was read, set its style in the
        # new cell.
        if old_xf_idx is not None:
            row._Row__cells[col].xf_idx = old_xf_idx

    # These methods are forwarded to the reader instance.
    for attr in ('set_row_index', 'set_sheet', 'next', 'next_not_empty'):
        locals()[attr] = _reader_delegator(attr)
    del attr

    def save(self, filename):
        self.book.save(filename)

    def close(self):
        pass

    def _set_packed_value(self, row, col, value):
        l = len(value)
        if l == 3:
            return row.set_cell_date(col, date(*value), self.date_fmt)
        elif l == 6:
            return row.set_cell_date(col, datetime(*value), self.datetime_fmt)
        raise ValueError(
            u'lists/tuples must have 3 or 6 elements (%r).' % value
        )

    def _get_format_and_value_for_date_cell(self, cell):
        value = cell.value
        if value == int(value):
            return (self.date_fmt, value)
        return (self.datetime_fmt, value)


def read(filename):
    return handle_xls_with_json(filename, XLSReader, sys.stdin, sys.stdout)

def write(filename, limit, **kw):
    return handle_xls_with_json(
        filename, XLSWriter, sys.stdin, sys.stdout, limit, **kw
    )

def handle_xls_with_json(filename, handler_type, input, output, *hargs, **hkw):
    """Handle a XLS file with commands and arguments passed as JSON
    arrays.

    A command is a method in a `handler_type` instance, and is always
    the first element in a JSON array.  The other elements are
    parameters passed to the method.
    """

    handler = handler_type(filename, *hargs, **hkw)
    readline = input.readline
    load = json.loads
    dump = json.dumps

    while True:
        line = readline().rstrip()
        args = load(line)

        if args is None:
            break

        if not isinstance(args, (tuple, list)) or len(args) < 1:
            raise ValueError, 'wrong argument %r' % line

        response = getattr(handler, args[0])(*args[1:])
        if response is not None:
            output.write(dump(response) + '\n')
            output.flush()

    handler.close()
