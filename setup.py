from setuptools import setup, find_packages
import os

version = '1.0'

setup(name='tnseguros.dataflow',
      version=version,
      description="",
      long_description=open(os.path.join("src", "README.txt")).read() + "\n" +
                       open(os.path.join("src", "docs", "HISTORY.txt")).read(),
      # Get more strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      classifiers=[
        "Programming Language :: Python",
        ],
      keywords='',
      author='TN Tecnologia e Neg\xc3\xb3cios',
      author_email='ed@tecnologiaenegocios.com.br',
      url='http://www.tecnologiaenegocios.com.br',
      license='GPL',
      packages=find_packages('src'),
      package_dir = {'':'src'},
      namespace_packages=['tnseguros'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          'xlutils',
          'simplejson',
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
